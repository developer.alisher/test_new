import axios from 'axios';

const apiClient = axios.create({
    baseURL: 'http://localhost:3000/api',
    withCredentials: false,
    headers: {
      'Content-Type': 'application/json',
    },
  });

export default {
  fetchProduct(productId: string) {
    return apiClient.get(`/catalog/get/${productId}`);
  },
  updateProduct(productId: number, productData: object) {
    return apiClient.post(`/catalog/update/${productId}`, productData);
  },
  deleteProduct(productId: number) {
    return apiClient.delete(`/catalog/delete/${productId}`);
  },
};
