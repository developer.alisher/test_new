import { defineStore } from 'pinia';

export const useProductStore = defineStore('productStore', {
  state: () => ({
    products: []
  }),
  actions: {
    async fetchProducts() {
      const response = await fetch('/api/catalog/get/all');
      this.products = await response.json();
    },
    async fetchProduct(productId) {
      const response = await fetch(`/api/catalog/get/${productId}`);
      return await response.json();
    },
    async updateProduct(productId, productData) {
      await fetch(`/api/catalog/update/${productId}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(productData)
      });
      const index = this.products.findIndex(product => product.id === productId);
      if (index !== -1) {
        this.products[index] = { ...this.products[index], ...productData };
      }
    },
    async deleteProduct(productId) {
      await fetch(`/api/catalog/delete/${productId}`, { method: 'DELETE' });
      this.products = this.products.filter(product => product.id !== productId);
    }
  }
});
