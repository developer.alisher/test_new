import { defineEventHandler } from 'h3';

export default defineEventHandler(async (event) => {
  const method = event.req.method;

  if (!event.req.url) return;

  const url = new URL(event.req.url, `http://${event.req.headers.host}`);
  const productId = url.searchParams.get('product_id');

  let mockProducts = [
    { id: 123, name: 'Бумага SvetoCopy', price: '3500' },
    { id: 124, name: 'Бумага SvetoCopy Eco', price: '4500' }
  ];

  if (method === 'GET') {
    if (productId) {
      return mockProducts.find(product => product.id === Number(productId));
    } else {
      return mockProducts;
    }
  } else if (method === 'POST' && productId) {
    let body = '';
    await new Promise(resolve => {
      event.req.on('data', chunk => body += chunk);
      event.req.on('end', resolve);
    });
    const productData = JSON.parse(body);

    const index = mockProducts.findIndex(product => product.id === Number(productId));
    if (index !== -1) {
      mockProducts[index] = { ...mockProducts[index], ...productData };
      return mockProducts[index];
    }
  } else if (method === 'DELETE' && productId) {
    mockProducts = mockProducts.filter(product => product.id !== Number(productId));
    return { message: 'Product deleted' };
  }
});
