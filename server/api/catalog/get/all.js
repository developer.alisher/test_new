import { defineEventHandler } from 'h3';

export default defineEventHandler(() => {
  const products = [
    { id: 123, name: 'Бумага SvetoCopy', price: '3500' },
    { id: 124, name: 'Бумага SvetoCopy Eco', price: '4500' }
  ];

  return products;
});
