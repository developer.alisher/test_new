export default {
  devtools: { enabled: true },
  plugins: ['~/plugins/pinia.js'],
  router: {
    routes: [
      {
        path: '/',
        name: 'Home',
        component: '@/pages/index.vue',
      },
      {
        path: '/products',
        name: 'Products',
        component: '@/pages/products.vue',
      },
      {
        path: '/products/:id',
        name: 'productEdit',
        component: '@/pages/edit.vue',
      },
    ],
  },
}

